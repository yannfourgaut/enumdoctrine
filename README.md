#Autoloading de configuration de champs de type enum pour Doctrine.

Avec l'utilisation de cet autoloading, une simple class de déclaration des champs de type enum, placés dans le dossier /config/AllEnums et respectant le CamelCase suffit.
Voir le fichier YannFourgaut\EnumDoctrine\Doctrine\AllEnums\Example en exemple
Il suffit ensuite de déclarer dans votre entity /** @Column(type="yourenum") */

##bootstrap.php 
	
	$allEnums = new \YannFourgaut\EnumDoctrine\Doctrine\ListEnums;
	foreach($allEnums->files as $class => $namespace){
		Type::addType( $class, $namespace);
	}

##Utilisation avec DoctrineOrmServiceprovider 
	// Appel de la liste des types enum à ajouter à Doctrine
	$allEnums = new \YannFourgaut\EnumDoctrine\Doctrine\ListEnums;

	// https://github.com/dflydev/dflydev-doctrine-orm-service-provider
	$app->register(new Dflydev\Provider\DoctrineOrm\DoctrineOrmServiceProvider, [
  	'orm.proxies_dir'             => 'src/Entity/Proxy',
  	'orm.auto_generate_proxies'   => $app['debug'],
  	'orm.em.options'              => [
  		'connection' => 'main',
    	'mappings' => [
      	[
        	'type'                         => 'annotation',
        	'namespace'                    => 'src\\Entity\\',
        	'path'                         => 'src/Entity',
        	'use_simple_annotation_reader' => false,
      	],
    	],
    	'types' => 
      		$allEnums->files
    	,
  	]
	]);

##Doctrine schema-tool:update

  mysql> use mydatabase

  Database changed

  mysql> SHOW TABLES;

  | Tables_in_mydatabase |
  | ---------------------|
  |example               |        


  mysql> SHOW COLUMNS FROM example;

  Field | Type              | Null | Key | Default | Extra          
  ------|-------------------|------|-----|---------|----------------
  id    | int(11)           | NO   | PRI | NULL    | auto_increment 
  enum  | enum('foo','bar') | NO   |     | NULL    |                

