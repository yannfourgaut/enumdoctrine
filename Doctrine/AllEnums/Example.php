<?php 

namespace YannFourgaut\EnumDoctrine\Doctrine\AllEnums;

use \YannFourgaut\EnumDoctrine\Doctrine\EnumType as EnumType;

/**
 * Class de validation de données transmises aux champs de type enum non supportés par Doctrine
 *
 * Ceci est un exemple, pour vos fichiers placés dans config/AllEnums, déclarer le namespace config\AllEnums 
 *
 * @author Yann Fourgaut <contact@yannfourgaut.com> 
 */

class Example extends EnumType{

	protected $name = 'Example';
    public $values = array('foo', 'bar');

}