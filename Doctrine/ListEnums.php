<?php 

namespace YannFourgaut\EnumDoctrine\Doctrine;

/**
 * Class de listing des types enum personnalisé pour Doctrine CLI et serveur
 *
 * Placer ces fichiers dans /config/AllEnums
 *
 * @author Yann Fourgaut <contact@yannfourgaut.com>
 */

class ListEnums{

	public $files = array();

	public $cli = array();

	public function __construct(){

		$dir = realpath(__DIR__.'/../../../../config/AllEnums');

		if ($dh = opendir($dir)) {
	        while ( ($file = readdir($dh) ) !== false) {

	            if( is_file($dir.'/'.$file) && $file !== '.' && $file !== '..'){

	            	include_once($dir.'/'.$file);
	            	
	            	$file = str_replace('.php','',$file);
	            	$this->files[ $file ] = '\\config\\AllEnums\\'.$file;
	            	$this->cli[ $file ] = '\\\\config\\\\AllEnums\\\\'.$file;
	            }

	        }
	        closedir($dh);
	    }

	}

}